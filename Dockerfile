FROM python:3.6.8-alpine3.10

# Install AWScli v2
ENV GLIBC_VER=2.31-r0
ENV SCEPTRE_VERSION=v2.3.0
ENV AWSCLI_VERSION=1.18.53

# install glibc compatibility for alpine
RUN apk update && apk --no-cache add \
        binutils \
        curl \
        git \
        make \
        openssh \
        yarn \
        nodejs \ 
        bash \
    && apk add --no-cache --virtual .build-deps \
    && curl -sL https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub -o /etc/apk/keys/sgerrand.rsa.pub \
    && curl -sLO https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VER}/glibc-${GLIBC_VER}.apk \
    && curl -sLO https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VER}/glibc-bin-${GLIBC_VER}.apk \
    && apk add --no-cache \
        glibc-${GLIBC_VER}.apk \
        glibc-bin-${GLIBC_VER}.apk 

# Set timezone to UTC by default
RUN ln -sf /usr/share/zoneinfo/Etc/UTC /etc/localtime

# Install aws-cli
RUN apk -Uuv add groff less jq
RUN pip install awscli==${AWSCLI_VERSION} 

RUN rm /var/cache/apk/*

#Clean up
RUN   apk --no-cache del \
        binutils \
        make \
        yarn \
        nodejs \
    && rm glibc-${GLIBC_VER}.apk \
    && rm glibc-bin-${GLIBC_VER}.apk \
    && rm -rf /var/cache/apk/*


WORKDIR /project
